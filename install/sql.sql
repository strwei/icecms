﻿DROP TABLE IF EXISTS `icebbs_accessory`;
CREATE TABLE `icebbs_accessory` (
  `accessory_id` int(11) NOT NULL AUTO_INCREMENT,
  `accessory_destination` varchar(255) DEFAULT NULL,
  `accessory_time` datetime DEFAULT NULL,
  `accessory_post_id` int(11) DEFAULT NULL,
  `accessory_post_if_money` int(1) DEFAULT '0',
  `accessory_post_money` int(11) DEFAULT NULL,
  `accessory_ps` varchar(240) DEFAULT '无备注',
  `accessory_ower_id` int(11) DEFAULT NULL,
  `accessory_down_times` int(11) DEFAULT '0',
  PRIMARY KEY (`accessory_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `icebbs_admin`;
CREATE TABLE `icebbs_admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_name` varchar(30) DEFAULT NULL,
  `admin_sid` varchar(255) DEFAULT NULL,
  `admin_login_last_ip` varchar(40) DEFAULT NULL,
  `admin_login_ip` varchar(40) DEFAULT NULL,
  `admin_login_time` datetime DEFAULT NULL,
  `admin_last_login_time` datetime DEFAULT NULL,
  `admin_password` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


INSERT INTO `icebbs_admin` VALUES (1,'admin','b11e61f8ebfda52a9a2f705b90cf2371','::1','::1','2015-08-12 20:30:54','2015-07-19 00:00:00','e10adc3949ba59abbe56e057f20f883e');

DROP TABLE IF EXISTS `icebbs_article`;
CREATE TABLE `icebbs_article` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT,
  `article_title` varchar(45) DEFAULT NULL,
  `article_ower_id` int(11) DEFAULT NULL,
  `article_content` longtext,
  `article_date` datetime DEFAULT NULL,
  `article_board_id` int(3) DEFAULT NULL,
  `article_sort` varchar(20) DEFAULT NULL,
  `article_hot` int(11) DEFAULT '0' COMMENT '0',
  `article_status` int(1) DEFAULT '3',
  `article_ower_name` varchar(20) DEFAULT NULL,
  `article_read_time` datetime DEFAULT NULL,
  `article_ip` varchar(35) DEFAULT NULL,
  `article_praise_times` int(11) DEFAULT '0',
  `article_lookdown_times` int(11) DEFAULT '0',
  `article_respond_time` datetime DEFAULT NULL,
  PRIMARY KEY (`article_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `icebbs_article_board`;
CREATE TABLE `icebbs_article_board` (
  `article_board_id` int(2) NOT NULL AUTO_INCREMENT,
  `article_board_name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`article_board_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `icebbs_article_respond`;
CREATE TABLE `icebbs_article_respond` (
  `respond_id` int(11) NOT NULL AUTO_INCREMENT,
  `respond_content` varchar(10000) DEFAULT NULL,
  `respond_time` datetime DEFAULT NULL,
  `respond_ip` varchar(40) DEFAULT NULL,
  `respond_status` int(1) DEFAULT '1',
  `respond_user_id` int(11) DEFAULT NULL,
  `respond_article_id` int(11) DEFAULT NULL,
  `respond_user_name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`respond_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `icebbs_article_set`;
CREATE TABLE `icebbs_article_set` (
  `article_set_id` int(2) NOT NULL AUTO_INCREMENT,
  `article_page_words` int(11) DEFAULT '1000',
  `article_mostwords` int(11) DEFAULT '100000',
  `article_if_ubb` int(1) DEFAULT '1',
  PRIMARY KEY (`article_set_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `icebbs_atmessage`;
CREATE TABLE `icebbs_atmessage` (
  `atmessage_id` int(11) NOT NULL AUTO_INCREMENT,
  `atmessage_from_user_name` varchar(20) DEFAULT NULL,
  `atmessage_from_user_id` int(11) DEFAULT NULL,
  `atmessage_to_user_name` varchar(20) DEFAULT NULL,
  `atmessage_to_user_id` int(11) DEFAULT NULL,
  `atmessage_if_read` int(1) DEFAULT '0',
  `atmessage_send_time` datetime DEFAULT NULL,
  `atmessage_content` varchar(255) DEFAULT NULL,
  `atmessage_postid` int(11) DEFAULT NULL,
  `atmessage_title` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`atmessage_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `icebbs_bbs_board`;
CREATE TABLE `icebbs_bbs_board` (
  `bbs_board_id` int(2) NOT NULL AUTO_INCREMENT,
  `bbs_board_name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`bbs_board_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `icebbs_bbs_post`;
CREATE TABLE `icebbs_bbs_post` (
  `post_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_title` varchar(40) DEFAULT NULL,
  `post_content` varchar(10000) DEFAULT NULL,
  `post_ower_id` int(11) DEFAULT NULL,
  `post_date` datetime DEFAULT NULL,
  `post_board_id` int(11) DEFAULT NULL,
  `post_sort` varchar(15) DEFAULT NULL,
  `post_hot` int(11) DEFAULT '0',
  `post_status` int(1) DEFAULT '1',
  `post_ower_name` varchar(16) DEFAULT NULL,
  `post_read_time` datetime DEFAULT NULL,
  `post_respond_time` datetime DEFAULT NULL,
  `post_ip` varchar(35) DEFAULT NULL,
  PRIMARY KEY (`post_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;


INSERT INTO `icebbs_bbs_post` VALUES (1,'ICECMS欢迎你的使用','@作者://QQ24722645<br />\r\n@官网:http://strwei.com<br />\r\n@使用协议:免费开源，自由修改<br />\r\n@此程序非常简单，所以仅仅供大家玩耍<br /><br />\r\n安装教程:(不支持sae，ACE)<br />\r\n第一步：把根目录下的config.php的数据库信息配置好<br />\r\n第二步：把根目录下的sql.sql导入数据库即可<br />\r\n这样就可以完成啦（后台地址/admin.php，初始账号admin,密码123456）<br /><br /><br />\r\n常见问题:<br />\r\n1.自带两套模板:wap2（手机页面）,web（电脑页面），自行识别手机和电脑页面（如果不需要电脑页面，可以到\\Home\\Home\\Controller<br /><br />\r\n\\IndexController.class.php里面把<br />\r\nif($_COOKIE[\'think_template\']==\'wap2\'){<br />\r\nif(!isMobile()){<br />\r\nheader( \"HTTP/1.1 301 Moved Permanently\"); <br />\r\nheader(\"Location: /index.php/home/Index/index/t/web\");<br />\r\n}<br />\r\n}<br />\r\nif($_COOKIE[\'think_template\']==\'web\'){<br />\r\nif(isMobile()){<br />\r\n@header( \"HTTP/1.1 301 Moved Permanently\"); <br />\r\n@header(\"Location: /index.php/home/Index/index/t/wap2\");<br />\r\n}<br /><br />\r\n这些代码删除<br />\r\n）<br /><br />\r\n2.<br />\r\n后台模板位于：\\Admin\\Home\\View目录下，<br />\r\n前台模板位于：\\Home\\Home\\View目录下，（如果会一点html,就很容易修改模板）<br /><br /><br />\r\n3.为什么用户点击找回密码却收不到邮件<br />\r\n解：找回密码需要用到邮箱的stmp（在Home\\Common\\Common\\function.php的第126行开始配置）<br /><br />\r\n4.怎么排版页面<br />\r\n解：请自行到前台后者后台模板目录下自行修改<br /><br />\r\n如果还有什么问题，漏洞等，大家可以提交<br />',1,'2015-08-13 20:51:11',4,NULL,1,1,'管理员','2015-08-13 20:57:53','2015-08-13 20:51:11','::1'),(2,'ICE说，他还没找到BUG','其实有很多BUG的，找到的话告诉他一声哦',1,'2015-08-13 20:53:45',3,NULL,1,1,'管理员','2015-08-13 20:53:56','2015-08-13 20:53:45','::1');

DROP TABLE IF EXISTS `icebbs_bbs_respond`;
CREATE TABLE `icebbs_bbs_respond` (
  `respond_id` int(11) NOT NULL AUTO_INCREMENT,
  `respond_content` varchar(2000) DEFAULT NULL,
  `respond_time` datetime DEFAULT NULL,
  `respond_ip` varchar(35) DEFAULT NULL,
  `respond_status` int(1) DEFAULT '1',
  `respond_user_id` int(11) DEFAULT NULL,
  `respond_post_id` int(11) DEFAULT NULL,
  `respond_user_name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`respond_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `icebbs_bbs_vote`;
CREATE TABLE `icebbs_bbs_vote` (
  `bbs_vote_id` int(11) NOT NULL AUTO_INCREMENT,
  `bbs_vote_content` varchar(30) DEFAULT NULL,
  `bbs_vote_user_id` varchar(5000) DEFAULT NULL,
  `bbs_vote_post_id` int(11) DEFAULT NULL,
  `bbs_vote_date` datetime DEFAULT NULL,
  `bbs_vote_number` int(11) DEFAULT '0',
  PRIMARY KEY (`bbs_vote_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `icebbs_chat`;
CREATE TABLE `icebbs_chat` (
  `chat_id` int(11) NOT NULL AUTO_INCREMENT,
  `chat_content` varchar(1000) DEFAULT NULL,
  `chat_user_id` int(11) DEFAULT NULL,
  `chat_user_name` varchar(20) DEFAULT NULL,
  `chat_time` datetime DEFAULT NULL,
  `chat_status` int(1) DEFAULT '1',
  PRIMARY KEY (`chat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `icebbs_message`;
CREATE TABLE `icebbs_message` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `message_from_name` varchar(20) DEFAULT NULL,
  `message_from_id` int(11) DEFAULT NULL,
  `message_to_name` varchar(20) DEFAULT NULL,
  `message_to_id` int(11) DEFAULT NULL,
  `message_if_read` int(1) DEFAULT '0',
  `message_send_time` datetime DEFAULT NULL,
  `message_if_email` int(1) DEFAULT '0',
  `message_content` varchar(10000) DEFAULT NULL,
  PRIMARY KEY (`message_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `icebbs_post_set`;
CREATE TABLE `icebbs_post_set` (
  `post_set_id` int(2) NOT NULL AUTO_INCREMENT,
  `post_page_words` int(8) DEFAULT '1000',
  `post_mostwords` int(8) DEFAULT '100000',
  `post_if_ubb` int(1) DEFAULT '1',
  PRIMARY KEY (`post_set_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `icebbs_user`;
CREATE TABLE `icebbs_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(20) DEFAULT NULL,
  `user_email` varchar(35) DEFAULT NULL,
  `user_phone` varchar(15) DEFAULT NULL,
  `user_sex` varchar(6) DEFAULT '未知',
  `user_reg_date` datetime DEFAULT NULL,
  `user_img` varchar(100) DEFAULT NULL,
  `user_sid` varchar(150) DEFAULT NULL,
  `user_status` int(1) DEFAULT '1',
  `user_act_time` datetime DEFAULT NULL,
  `user_character` varchar(255) DEFAULT NULL,
  `user_location` varchar(50) DEFAULT NULL,
  `user_born_date` date DEFAULT NULL,
  `user_reg_ip` varchar(30) DEFAULT NULL,
  `user_login_last_ip` varchar(30) DEFAULT NULL,
  `user_login_ip` varchar(30) DEFAULT NULL,
  `user_violations_content` varchar(255) DEFAULT NULL,
  `user_money` int(11) DEFAULT '100',
  `user_sign_in` int(1) DEFAULT '0',
  `user_theme_number` int(6) DEFAULT '0',
  `user_respond_time` bigint(20) DEFAULT '0',
  `user_announce_time` bigint(20) DEFAULT '0',
  `user_say_time` bigint(20) DEFAULT '0',
  `user_message_time` bigint(20) DEFAULT '0',
  `user_state` int(1) DEFAULT '0',
  `user_password` varchar(60) DEFAULT NULL,
  `user_login_time` datetime DEFAULT NULL,
  `user_last_login_time` datetime DEFAULT NULL,
  `user_integral` int(11) DEFAULT '0',
  `user_respond_number` int(6) DEFAULT '0',
  `user_rank` varchar(20) DEFAULT NULL,
  `user_respond_a_time` bigint(20) DEFAULT NULL,
  `user_zone` longtext,
  `user_token` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=99 DEFAULT CHARSET=utf8;


INSERT INTO `icebbs_user` VALUES (1,'管理员','24722645@qq.com','13250479087','男','0000-00-00 00:00:00',NULL,'40ad3f334c270691fba560029a7e0b93',1,NULL,'',NULL,NULL,NULL,'::1','::1',NULL,569,0,11,1438430923,1439470425,0,0,0,'e10adc3949ba59abbe56e057f20f883e','2015-08-03 15:19:55','2015-08-03 15:19:28',600,67,'',NULL,'<script language =\"javascript\" type =\"text/javascript\">\r\n\r\nfunction a(){\r\n         var acookie=document.cookie\r\n\t            alert(acookie);\r\n     }\r\n     </script>\r\n<a a href=javascript:onclick=a() return false;>点击</a>\r\n<div></div>','2552ea22190bf61d182e2b0e32142f5a');


DROP TABLE IF EXISTS `icebbs_website`;
CREATE TABLE `icebbs_website` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `status_status` int(1) DEFAULT '1',
  `status_title` varchar(255) DEFAULT 'icebbs welcome you',
  `status_describe` varchar(255) DEFAULT '用异步的方式编程',
  `status_announce` varchar(255) DEFAULT '网站正在维护中。。。。',
  `status_ddos` int(1) DEFAULT '1',
  `status_post_number` int(11) DEFAULT '1000',
  `status_user_timeout` int(11) DEFAULT '3600',
  `status_user_record_online` int(1) NOT NULL DEFAULT '1',
  `status_ddos_times` int(3) DEFAULT '10',
  `reg_method` int(1) DEFAULT '1',
  `status_key` varchar(100) DEFAULT NULL,
  `s_integral` int(5) DEFAULT '20',
  `s_money` int(5) DEFAULT '15',
  `r_money` int(5) DEFAULT '4',
  `r_integral` int(11) DEFAULT '6',
  `f_maxsize` int(11) DEFAULT '3145728',
  `website_foot` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


INSERT INTO `icebbs_website` VALUES (1,0,'ICECMS','ICECMS欢迎各路大神入住','',1,1000,3600,1,10,1,'ice,php,c,c++,编程',20,15,4,5,0,'<div class=\"title2\">酷站推荐<a href=\"./?action=link&vs=1\"><span class=\"right\">申请</span></a></div>\r\n<div class=\"link\">\r\n<a href=\"\">友链</a>\r\n</div>');