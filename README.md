#icecms


使用协议:MIT,免费开源，自由修改

此程序非常简单，所以仅仅供大家玩耍

安装教程:(不支持sae，ACE)

第一步：把根目录下的config.php的数据库信息配置好

第二步：把根目录下的sql.sql导入数据库即可

这样就可以完成啦（后台地址/admin.php，初始账号admin,密码123456）


常见问题:

1.自带两套模板:wap2（手机页面）,web（电脑页面），自行识别手机和电脑页面（如果不需要电脑页面，可以到\Home\Home\Controller\IndexController.class.php里面把
if($_COOKIE['think_template']=='wap2'){
			if(!isMobile()){
				header( "HTTP/1.1 301 Moved Permanently"); 
				header("Location: /index.php/home/Index/index/t/web");
			}
		}
		if($_COOKIE['think_template']=='web'){
			if(isMobile()){
				@header( "HTTP/1.1 301 Moved Permanently"); 
				@header("Location: /index.php/home/Index/index/t/wap2");
			}

这些代码删除
）

2.
后台模板位于：\Admin\Home\View目录下，
前台模板位于：\Home\Home\View目录下，（如果会一点html,就很容易修改模板）


3.为什么用户点击找回密码却收不到邮件
解：找回密码需要用到邮箱的stmp（在Home\Common\Common\function.php的第126行开始配置）

4.怎么排版页面
解：请自行到前台后者后台模板目录下自行修改

